package solution;

import java.util.HashSet;

public class Solution {
	public static int lengthOfLongestSubstring(String s) {
		int fast=0, slow=0, maxLength=0;
		HashSet<Character> box = new HashSet<Character>();
		
		if (s == null || s.length() == 0) return 0;
		
		for(fast=0; fast < s.length(); fast++) {
			if(!box.contains(s.charAt(fast))) {
				box.add(s.charAt(fast));
				maxLength = Math.max(maxLength,fast-slow+1);
			} else {
				while(slow < fast && s.charAt(slow) != s.charAt(fast)) {
					box.remove(s.charAt(slow));
					slow++;
				}
				slow ++;
			}
		}
		return maxLength;
	}
	
	public static void main(String[] args) {
		String s = "dfaetgewe";
		
		int maxLength = lengthOfLongestSubstring(s);
		
		System.out.println("Maximum length is "+maxLength);
	}
}
