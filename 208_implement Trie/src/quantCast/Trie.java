package quantCast;

import java.util.ArrayList;
import java.util.List;

public class Trie {
	TrieNode root;
	/** Initialize your data structure here. */
    public Trie() {
        root = new TrieNode();
    }
    
    /** Inserts a word into the trie. */ 
    /*
     * Straightforward. For each char in word and starting
     * from root of Trie, first check whether current node's
     * children is the char. If that child does not exist, create
     * that child and add word to that child's words.
     * At the leaves, set isEnd to true
     */
    public void insert(String word) {
        TrieNode curr = root;
        for(int i = 0; i < word.length(); i++) {
        	int idx = word.charAt(i)-'a';
        	if(curr.children[idx] == null) {
        		curr.children[idx] = new TrieNode();
        	}
        	curr.children[idx].words.add(word);
        	curr = curr.children[idx];
        }
        curr.isEnd = true;
    }
    
    public List<String> searchPrefix(String prefix) {
    	TrieNode curr = root;
    	for(int i=0; i<prefix.length(); i++) {
    		int idx = prefix.charAt(i)-'a';
    		if(curr.children[idx] == null) {
    			return new ArrayList<String>();
    		}
    		curr = curr.children[idx];
    	}
    	return curr.words;
    }
    
    /** Returns if the word is in the trie. */
    public boolean search(String word) {
        List<String> words = searchPrefix(word);
        return words.contains(word);
    }
    
    /** Returns if there is any word in the trie that starts with the given prefix. */
    public boolean startsWith(String prefix) {
        List<String> words = searchPrefix(prefix);
        return words.isEmpty();
    }
}
