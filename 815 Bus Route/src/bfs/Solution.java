package bfs;

import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

public class Solution {
	public static int numBusesToDestination(int[][] routes, int S, int T) {
		HashSet<Integer> routeTaken = new HashSet<>();//store bus route you have taken
		Deque<Integer> stopArrived = new LinkedList<>();//Bus stop #
		HashMap<Integer, ArrayList<Integer>> routeMap = new HashMap<>();
		int res = 0;
		
		if(S == T) {
			return res;
		}
		
		/*
		 * At each stop, we want to know what routes we can take there
		 * For each stopId in the initialization step, we first retrieve 
		 * the routeIdSet already added to that stopId. If none, create a
		 * new ArrayList. Then add new routeId to the routeIdSet.
		 */
		for(int routeId = 0; routeId < routes.length; routeId++) {
			for(int stopId: routes[routeId]) {
				ArrayList<Integer> routeIdSet = routeMap.getOrDefault(stopId, new ArrayList<>());
				routeIdSet.add(routeId);
				routeMap.put(stopId, routeIdSet);
			}
		}
		
		stopArrived.offer(S); //the root node, offer returns boolean, while addFirst returns void
		while(!stopArrived.isEmpty()) {
			int size = stopArrived.size();
			res++;
			for(int i = 0; i<size; i++) {
				int cur = stopArrived.pollFirst(); //poll() is the same
				ArrayList<Integer> routeIdSet = routeMap.get(cur);//at current stop, what routes you can take
				for(int routeId: routeIdSet) {
					if(routeTaken.contains(routeId)) continue;
					routeTaken.add(routeId);
					
					/*
					 * to see on current bus, which stops you can arrive
					 */
					for(int stopId: routes[routeId]) {
						if(stopId == T) return res;
						stopArrived.offer(stopId);//add children nodes to current parent node
					}
				}
			}
		}	
		return -1;
	}
	
	public static void main(String[] args) {
		int[][] routes = {{1,2,7}, {3,6,7}};
		int S = 1, T = 6;
		
		int res = numBusesToDestination(routes, S, T);
		System.out.println("The least number of buses we have to take is "+res);
	}
}
