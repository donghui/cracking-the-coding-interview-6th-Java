import java.util.Stack;
import java.util.ArrayList;
import java.util.List;

public class Solution{
  public static boolean canVisitAllRooms(List<List<Integer>> rooms){
    boolean[] visitedRooms = new boolean[rooms.size()];
    visitedRooms[0] = true;
    Stack<Integer> stack = new Stack<Integer>();
    stack.push(0);
    
    while(!stack.isEmpty()){
      int currentRoom = stack.pop();
      for(int key: rooms.get(currentRoom)){
        if(!visitedRooms[key]){
          visitedRooms[key] = true;
          stack.push(key);
        }
      }
    }
    
    for(boolean v: visitedRooms){
      if(!v) return false;
    }
    return true;
  }
  
  public static void main(String[] arges){
    List<List<Integer>> rooms = new ArrayList<List<Integer>>();
    List<Integer> list = new ArrayList<Integer>();
    list.add(1);
    rooms.add(new ArrayList<Integer>(list));
    
    list.remove(0); list.add(2);
    rooms.add(new ArrayList<Integer>(list));
    
    list.remove(0);
    rooms.add(new ArrayList<Integer>(list));
    
    if(canVisitAllRooms(rooms)){
      System.out.println("True!");
    }else{
      System.out.println("False!");
    }
  }
}