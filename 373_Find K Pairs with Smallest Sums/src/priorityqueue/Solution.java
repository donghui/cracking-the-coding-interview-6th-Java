package priorityqueue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;

public class Solution {
	public static List<int[]> kSmallestPairs(int[] nums1, int[] nums2, int k) {
        List<int[]> res = new ArrayList<int[]>();
        
        if(nums1 == null || nums2 == null || nums1.length == 0 || nums2.length == 2) {
        		return res;
        }
        
        SumComparator comp = new SumComparator(nums1, nums2);
        PriorityQueue<int[]> pq = new PriorityQueue<>(comp);
        
        for(int i = 0; i < nums1.length; i++) {
	        	/*
	        	 * o1 and o2 are {i,0}, pq will compare
	        	 * nums1[i]+nums2[0], nums1[0]+nums2[i]
	        	 * then first put {0, i} or {i, 0} based on
	        	 * comparison.
	        	 */
	        	pq.offer(new int[]{i, 0});
        }
        
        while(k > 0 && !pq.isEmpty()) {
	        	int[] idx = pq.poll();
	        	res.add(new int[]{nums1[idx[0]], nums2[idx[1]]});
	        	if(idx[1]<nums2.length-1) {
	        		pq.offer(new int[] {idx[0], idx[1]+1});
	        	}
	        	k--;
        }
        return res;
    }
	
	public static void main(String[] args) {
		int[] nums1 = {1,3,4,7,8};
		int[] nums2 = {1,2,3,4};
		List<int[]> res = kSmallestPairs(nums1, nums2, 3);
		for (int[] elem: res) {
			System.out.println(Arrays.toString(elem));
		}
	}
}
