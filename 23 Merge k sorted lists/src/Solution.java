

public class Solution {
	public static ListNode mergeKLists(ListNode[] lists) {
		if (lists == null || lists.length == 0) {
			return null;
		}
		
		return mergeKLists(lists, 0, lists.length-1);
	}

	private static ListNode mergeKLists(ListNode[] lists, int left, int right) {
		if (left < right) {
			int mid = left+(right-left)/2;
			return merge(mergeKLists(lists, left, mid), mergeKLists(lists, mid+1, right));
		}
		return lists[left];
	}
	
	private static ListNode merge(ListNode l1, ListNode l2) {
        ListNode results = new ListNode(0);
        ListNode curr = results;
        while (l1 != null && l2 != null) {
            if (l1.val < l2.val) {
                curr.next = l1;
                l1 = l1.next;
            } else {
                curr.next = l2;
                l2 = l2.next;
            }
            curr = curr.next;
        }
        if (l1 == null) {
            curr.next = l2;
        }
        if (l2 == null) {
            curr.next = l1;
        }
        return results.next;
    }
	
	public static void main (String[] args) {
		ListNode list1 = new ListNode(1);
		list1.next = new ListNode(4);
		list1.next = new ListNode(5);
		
		ListNode list2 = new ListNode(1);
		list2.next = new ListNode(3);
		list2.next = new ListNode(4);
		
		ListNode list3 = new ListNode(2);
		list3.next = new ListNode(6);
		
		ListNode[] lists = {list1, list2, list3};
		
		ListNode result = mergeKLists(lists);
		
		while(result != null) {
			System.out.println(result.val);
			result = result.next;
		}
	}
}
