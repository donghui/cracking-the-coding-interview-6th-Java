package quantCast;

import java.util.ArrayList;
import java.util.List;

public class TrieNode {
	/*children is a must
	 * isEnd necessity based on function realized
	 * words depends on application
	 */
	boolean isEnd;
	TrieNode[] children;
	List<String> words;
	
	/*
	 * TrieNode can be initiated by
	 * TrieNode node = new TrieNode();
	 */
	public TrieNode() {
		children = new TrieNode[26];
		words = new ArrayList<String>();
	}
}
