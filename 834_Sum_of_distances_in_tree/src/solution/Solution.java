package solution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Solution {
	static int[] numNodes; //number of nodes of a subtree
	static int[] res; //dfs_1: res is distance of subtree, dfs_2: the final results
	static List<Set<Integer>> graph; //initiate parent-children sets
	static int N;
	
	public static int[] sumOfDistancesInTree(int n, int[][] edges) {
		N = n;
		numNodes = new int[N];
		res = new int[N];
		Arrays.fill(numNodes, 1); //view each node as a subtree with a single node
		graph = new ArrayList<Set<Integer>>();
		for(int i=0; i<N; i++) {
			graph.add(new HashSet<Integer>());
		}
		
		for(int[] edge:edges) {
			graph.get(edge[0]).add(edge[1]);
			graph.get(edge[1]).add(edge[0]);
		}
		
		dfs_1(0,-1);
		dfs_2(0,-1);
		
		return res;
	}
	
	//bottom-up, from leaves to root, update numNodes
	public static void dfs_1(int node, int parent) {
		for(int child: graph.get(node)) {
			if(child != parent) {
				dfs_1(child, node); //directly go to leaves
				numNodes[node] += numNodes[child];
				res[node] += res[child] + numNodes[child];//numNodes[child] equals to res[leaves]
			}
		}
	}
	
	//top-down
	public static void dfs_2(int node, int parent) {
		for(int child: graph.get(node)) {
			if(child != parent) {
				res[child] = res[node] -numNodes[child] +(N-numNodes[child]);
				dfs_2(child, node); //node transit from child to parent
			}
		}
	}
	
	public static void main (String[] args) {
		int N = 6;
		int[][] edges = {{0,1},{0,2},{2,3},{2,4},{2,5}};
		int[] results = sumOfDistancesInTree(N, edges);
		
		System.out.println("The distances are "+Arrays.toString(results));
		
	}
}
