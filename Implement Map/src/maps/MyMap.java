package maps;

public class MyMap {
	ListNode[] buckets;
	
	public MyMap() {
		buckets = new ListNode[100];
		for (int i = 0; i < buckets.length; i++) {
			buckets[i] = new ListNode(0,1);
		}
	}
	
	public void put(int key, int value) {
		int idx = key % 100;
		ListNode node = buckets[idx];
		while(node.next != null) {
			if(node.next.key == key) {
				//put
				node.next.value = value;
				return;
			}
			node = node.next;
		}
		node.next = new ListNode(key, value);
	}
	
	public int get(int key) {
		int idx = key%100;
		ListNode node = buckets[idx];
		while(node.next != null) {
			if(node.next.key == key) {
				//get
				return node.next.value;
			}
			node = node.next;
		}
		return 0;
	}
	
	public void delete(int key) {
		int idx = key % 100;
		ListNode node = buckets[idx];
		while(node.next != null) {
			if(node.next.key == key) {
				//delete
				node.next = node.next.next;
				return;
			}
			node = node.next;
		}
	}
}
