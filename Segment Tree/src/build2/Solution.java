package build2;

public class Solution {
	public static SegmentTreeNode build(int[] A) {
		return buildhelper(A, 0, A.length-1);
	}
	

	private static SegmentTreeNode buildhelper(int[] A, int start, int end) {
		if(start > end) return null;
		
		//max can be either A[start] or A[end]
		if(start == end) return new SegmentTreeNode(start, end, A[start]);
		
		int mid = start+(end-start)/2;
		SegmentTreeNode left = buildhelper(A, start, mid);
		SegmentTreeNode right = buildhelper(A, mid+1, end);
		
		SegmentTreeNode root = new SegmentTreeNode(start, end, Math.max(left.max, right.max));
		root.left = left;
		root.right = right;
		
		return root;
	}

	public static void main(String[] args) {
		int[] A = {2,1,7,5,4,3};
		SegmentTreeNode root = build(A);
		System.out.println(root.max);
		System.out.println(root.right.max);
	}
}
