package dsu;

import java.util.Arrays;

public class Solution {
	public static int[] hitBricks(int[][] grid, int[][] hits) {
		int R = grid.length; //# of rows
		int C = grid[0].length; //# of columns
		/*
		 * directions, bottom, right, top, left
		 */
		int[] dr = {1,0,-1,0}; 
		int[] dc = {0,1,0,-1};
		
		/*
		 * A is the grid after all hits
		 */
		int[][] A = new int[R][C];
		for(int r = 0; r < R; r++) {
			A[r] = grid[r].clone();
		}
		for(int[] hit: hits) {
			A[hit[0]][hit[1]] = 0;
		}
		
		/*
		 * dsu has RC+1 elements, as the last element connects all the
		 * bricks in the roof/ceiling/top.
		 * Attach top and left elements as DSU
		 */
		DSU dsu = new DSU(R*C+1);
		for(int r=0; r<R; r++) {
			for(int c=0; c<C; c++) {
				if(A[r][c]==1) {
					int i = r*C+c;				
					//attached all ceiling bricks to R*C^th node
					if(r == 0) dsu.union(i, R*C);
					//In the same column, if the top brick is there, won't fall
					if(r > 0 && A[r-1][c] == 1) dsu.union(i, (r-1)*C + c);
					//In the same row, add the left brick
					if(c > 0 && A[r][c-1] == 1) dsu.union(i, r*C + c-1);
				}
			}
		}
		int t = hits.length;
		int[] ans = new int[t];
		t--;//because the index of last element in ans is length-1
		
		/*
		 * From the last hit to the first hit
		 */
		while(t >= 0) {
			int r = hits[t][0]; //hit row index
			int c = hits[t][1]; //hit column index
			int preRoof = dsu.top();//total size of connected sets attached to ceiling
			if (grid[r][c] ==0) {
				t--; //bai qiao le
			} else {
				int i = r*C+c;
				for(int k = 0; k < 4; k++) {
					int nr = r + dr[k];
					int nc = c + dc[k];
					//the neighbor should not out of range and the neighbor has a brick
					if(0 <= nr && nr < R && 0 <= nc && nc < C && A[nr][nc] == 1) {
						dsu.union(i, nr*C + nc);
					}
				}
				if (r == 0) {
					dsu.union(i, R*C);
				}
				A[r][c] = 1;
				ans[t] = Math.max(0, dsu.top()-preRoof-1);
				t--;
			}
		}
		return ans;
	}
	
	public static class DSU{
		int[] parent, rank, sz;
		
		public DSU(int N) {
			/*
			 * At the beginning, suppose every brick is disjoint, then
			 * each brick is its own parent.
			 */
			parent = new int[N];
			for (int i = 0; i < N; i++) {
				parent[i] = i; 
			}
			rank = new int[N];
			sz = new int[N];
			Arrays.fill(sz, 1); //Initially, every set has 1 brick
		}
		
		/*
		 * for any x, find its parent, and its parent's parent, until
		 * the first ancestor is found.
		 * The first ancestor must exist, otherwise ancestor() will be 
		 * stuck in infinite loop
		 */
		public int ancestor(int x) {
			if (parent[x] != x) parent[x] = ancestor(parent[x]);
			return parent[x];
		}
		
		/*
		 * 
		 */
		public void union(int x, int y) {
			int xr = ancestor(x);
			int yr = ancestor(y);
			/*
			 * if x and y are already found to be connected, do not increase the size
			 * of that set
			 */
			if(xr == yr) return; //if they have the same first ancestor
			
			/*
			 * x is always at higher rank.
			 * In this case, rank is not necessary, but it's better to 
			 * keep this parameter.
			 */
			if(rank[xr] == rank[yr]) {
				rank[xr]++;
			}
			if(rank[xr] < rank[yr]) {
				int tmp = xr;
				xr = yr;
				yr = tmp;
			}
			parent[yr] = xr;
			sz[xr] += sz[yr];//Highest rank has the total size
		} 
		
		public int size(int x) {
			return sz[ancestor(x)];
		}
		
		/*
		 * sz.length-1 just means the last element
		 * Gives the size for R*C^th element, i.e. the Roof.
		 */
		public int top() {
			return size(sz.length-1) - 1;
		}
	}
	
	public static void main(String[] args) {
		int[][] grid = {{1,0,0,0},{1,1,1,0}};
		int[][] hits = {{1,0}};
		
		int[] res = hitBricks(grid, hits);
		System.out.println("Result is "+Arrays.toString(res));
	}
}
