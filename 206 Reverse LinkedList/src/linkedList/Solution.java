package linkedList;

public class Solution {
	public static ListNode reverseList(ListNode head) {
		/*
		 * Eliminate special cases
		 */
		if(head == null || head.next == null) {
			return head;
		}
		
		//pre->curr->next is the order in original linkedlist
		ListNode pre, curr, next;
		curr = head;
		pre = null;
		next = null;
		
		while(curr != null) {
			next = curr.next;
			curr.next = pre;
			pre = curr;
			curr = next;
		}
		
		return pre;//after while loop curr is null
	}
	
	public static void main(String[] args) {
		ListNode head = new ListNode(1);
		head.next = new ListNode(2);
		head.next.next = new ListNode(3);
		head.next.next.next = new ListNode(4);
		
		ListNode res = reverseList(head);
		
		for(int i=0; i<4; i++) {
			System.out.println(res.val);
			res = res.next;
		}
	}
}
