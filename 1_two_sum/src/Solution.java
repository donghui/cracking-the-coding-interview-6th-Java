import java.util.Arrays;
import java.util.HashMap;

public class Solution {
	public static int[] twoSum(int[] nums, int target) {
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		int[] result = new int[2];
		
		for(int i = 0; i < nums.length; i++) {
			if(map.containsKey(target-nums[i])) {
				int index = map.get(target-nums[i]);
				result[0] = index;
				result[1] = i;
				break;
			} else {
				map.put(nums[i], i);
			}
		}
		return result;
	}
	
	public static void main(String[] arges) {
		int[] nums = {2,7,11,15};
		int target = 9;
		int[] results = twoSum(nums, target);
		
		System.out.printf("Indices are ");
		System.out.println(Arrays.toString(results));
	}
}
