package list;

import java.util.PriorityQueue;

public class Solution {
    public static int kthSmallest(int[][] matrix, int k) {
        int n = matrix.length;
        PriorityQueue<Tuple> pq = new PriorityQueue<Tuple>();
        for(int j = 0; j <= n-1; j++) pq.offer(new Tuple(0, j, matrix[0][j]));
        for(int i = 0; i < k-1; i++) {
            Tuple t = pq.poll();
            if(t.x == n-1) continue;
            pq.offer(new Tuple(t.x+1, t.y, matrix[t.x+1][t.y]));
        }
        return pq.poll().val;
    }
    
    public static void main(String[] args) {
    	int[][] matrix = {{1,5,9},{10,11,13},{12,13,15}};
    	int res = kthSmallest(matrix, 8);
    	System.out.println("result is "+res);
    }
}
