package bfs;

import java.util.Deque;
import java.util.LinkedList;

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */

public class Solution {
	public static int widthOfBinaryTree(TreeNode root) {
		int res = 0;
		if (root == null) {
			return res;
		}
		Deque<TreeNode> parentQueue = new LinkedList<TreeNode>();
		Deque<Integer> parentIndex = new LinkedList<Integer>();
		
		parentQueue.offer(root);
		parentIndex.offer(1);
		
		while(!parentQueue.isEmpty()) {
			int size = parentQueue.size();
			int leftmost = 0;
			/*
			 * Loop over all parent nodes on each level
			 */
			for(int i=0; i<size; i++) {
				//poll the parent node out of the deque
				TreeNode parent = parentQueue.pollFirst();
				int idx = parentIndex.pollFirst();
				
				//Now we are at the leftmost node on certain level
				
				if(i == 0) {
					leftmost = idx;
				}
				
				//Now we are at the rightmost node on certain level
				if(i == size-1) {
					res = Math.max(res, idx-leftmost+1);
				}
				
				//Add left children node, whose index is 2*i
				if(parent.left != null) {
					parentQueue.offer(parent.left);
					parentIndex.offer(2*idx);
				}
				
				//Add right children node, whose index is 2*i+1
				if(parent.right != null) {
					parentQueue.offer(parent.right);
					parentIndex.offer(2*idx+1);
				}
			}
		}
		return res;
    }
	
	public static void main(String[] args) {
		//Result should be 3
//		TreeNode root = new TreeNode(4);
//		root.left = new TreeNode(9);
//		root.right = new TreeNode(1);
//		root.left.left = new TreeNode(3);
//		root.right.left = new TreeNode(2);
		
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(1);
		root.right = new TreeNode(1);
		root.left.left = new TreeNode(1);
		root.right.right = new TreeNode(1);
		root.left.left.left = new TreeNode(1);
		root.right.right.right = new TreeNode(1);
		System.out.println("The maximum width of the tree is "+widthOfBinaryTree(root));
	}
}
