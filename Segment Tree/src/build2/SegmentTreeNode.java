package build2;

public class SegmentTreeNode {
	int start, end, max;
	SegmentTreeNode left, right;
	SegmentTreeNode(int start, int end, int max){
		this.start = start;
		this.end = end;
		this.max = max;
		this.left = null;
		this.right = null;
	}
}
