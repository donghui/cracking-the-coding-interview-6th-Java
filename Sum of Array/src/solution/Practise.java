package solution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Practise {
	public static int[] sumArray(int[] arr1, int[] arr2) {
		if(arr1 == null && arr2 == null) {
			return new int[0];
		}
		
		if(arr1 == null || arr1.length == 0) {
			return arr2;
		}
		
		if(arr2 == null || arr2.length == 0) {
			return arr1;
		}
		
		List<Integer> list = new ArrayList<Integer>();
		int i = arr1.length-1;
		int j = arr2.length-1;
		int carry = 0;
		
		while(i >= 0 || j >= 0) {
			int val1 = (i<0? 0:arr1[i]);
			int val2 = (j<0? 0:arr2[j]);
			
			int sum = val1+val2+carry;
			carry = sum/10;
			list.add(sum%10);
			
			i--;
			j--;
		}
		
		if(carry > 0) list.add(carry);
		
		int[] res = new int[list.size()];
		for(int k=0; k < list.size(); k++) {
			res[list.size()-k-1] = list.get(k);
		}
		return res;
	}

	public static void main(String[] args) {
		//should output {1,3,0}
		int[] arr1 = new int[] {1,2,3};
		int[] arr2 = new int[] {7};
		
		int[] res = sumArray(arr1, arr2);
		System.out.println(Arrays.toString(res));
	}
}
