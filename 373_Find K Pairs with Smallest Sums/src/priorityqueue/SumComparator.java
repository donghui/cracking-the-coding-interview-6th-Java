package priorityqueue;

import java.util.Comparator;

public class SumComparator implements Comparator<int[]>{
	int[] nums1, nums2;
	
	public SumComparator(int[] nums1, int[] nums2) {
		this.nums1 = nums1;
		this.nums2 = nums2;
	}
	
	/*
	 * o1 and o2 have the same data type as int[] in 
	 * Comparator<int[]>
	 */
	@Override
	public int compare(int[] o1, int[] o2) {
		return (nums1[o1[0]]+nums2[o1[1]])-(nums1[o2[0]]+nums2[o2[1]]);
	}
	
}
