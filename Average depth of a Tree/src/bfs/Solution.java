package bfs;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class Solution {
	public static double findDepthAverage(TreeNode root) {
		if(root == null) {
			return 0;
		}
		
		List<Integer> depths = new ArrayList<Integer>();
		
		Deque<TreeNode> parentNodes = new LinkedList<TreeNode>();
		parentNodes.offer(root);
		
		/*
		 * BFS: breadth first search. for-loop loops over each level of the tree, because
		 * in each iteration, we poll the parent node (parent is deleted from deque) and 
		 * add all its children nodes. On each level, whenever we find a node is the leaf,
		 * we add the depth of current level to List depths.
		 */
		int depth = 0;
		while(!parentNodes.isEmpty()) {
			depth++;
			int size = parentNodes.size();
			for(int i = 0; i < size; i++) {
				TreeNode parent = parentNodes.pollFirst();
				
				//We find the node is a leaf on current level
				if(parent.left == null && parent.right == null) {
					depths.add(depth);
				}
				
				//We need proceed to left child in the next level
				if(parent.left != null) {
					parentNodes.offer(parent.left);
				}
				
				//We need to proceed to right child in the next level
				if(parent.right != null) {
					parentNodes.offer(parent.right);
				}
			}
		}
		
		int total = 0;
		for(int i: depths) {
			total += i;
		}
		return (double) total/depths.size();
	}

	public static void main(String[] args) {
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(1);
		root.right = new TreeNode(1);
		root.left.left = new TreeNode(1);
		root.right.right = new TreeNode(1);
		root.left.left.left = new TreeNode(1);
		root.right.right.right = new TreeNode(1);
		
		System.out.println("Average depth of the tree is "+findDepthAverage(root));
	}
}
