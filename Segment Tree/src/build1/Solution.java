package build1;

public class Solution {
	public static SegmentTreeNode build(int start, int end) {
		if(start>end) return null;
		
		SegmentTreeNode root = new SegmentTreeNode(start, end);
		if(start == end) {
			return root;
		}
		int mid = start+(end-start)/2;
		
		root.left = build(start, mid);
		root.right = build(mid+1, end);
		
		return root;
	}
	
	public static void main(String[] args) {
		int start = 0;
		int end = 5;
		SegmentTreeNode root = build(start, end);
		System.out.println(root.start);
		System.out.println(root.right.start);
	}
}
