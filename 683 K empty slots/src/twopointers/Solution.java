package twopointers;

public class Solution {
	public static int kEmptySlots(int[] flowers, int k) {
		//find the inverse function
		int[] days = new int[flowers.length];
		for(int i=0; i<flowers.length; i++) {
			days[flowers[i]-1] = i+1;
		}
		
		int left = 0;
		int right = k+1;
		int result = Integer.MAX_VALUE;
		for(int i=left; right<flowers.length; i++) {
			if(days[i]<=days[right] || days[i]<days[left]) {
				if (i == right) {
					result = Math.min(result, Math.max(days[left],  days[right]));
				}
				left = i;
				right = left+k+1;
			}
		}
		
		return (result == Integer.MAX_VALUE)? -1:result;
	}
	
	public static void main(String[] args) {
		int[] flowers = {1,3,2};
		int k = 1;
		
		int day = kEmptySlots(flowers, k);
		System.out.println("On the "+day+"th day, ");
	}

}
