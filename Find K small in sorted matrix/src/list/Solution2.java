package list;

import java.util.PriorityQueue;

public class Solution2 {
	public static int kthSmallest(int[][] matrix, int k) {
		PriorityQueue<IntComparator> pq = new PriorityQueue<IntComparator>();
		int ncol = matrix[0].length;
		int nrow = matrix.length;
		for(int irow = 0; irow < nrow; irow++) {
			for(int icol = 0; icol < ncol; icol++) {
				pq.offer(new IntComparator(matrix[irow][icol]));
			}
		}
		for(int i = 0; i < k-1; i++) {
			pq.poll();
		}
		return pq.poll().val;
	}
	
	public static void main(String[] args) {
    	int[][] matrix = {{1,4,7,11,15},{2,5,8,12,19},{3,6,9,16,22},{10,13,14,17,24},{18,21,23,26,30}};
//		int[}[] matrix = {{1,2},{1,3}};
    	int res = kthSmallest(matrix, 5);
    	System.out.println("result is "+res);
    }

}
