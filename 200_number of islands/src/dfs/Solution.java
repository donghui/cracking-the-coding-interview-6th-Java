package dfs;

class Solution {
    public static int numIslands(char[][] grid) {
        if(grid==null || grid.length==0){
            return 0;
        }
        
        int res = 0;
        int m=grid.length, n=grid[0].length;
        for(int i=0; i<m; i++){
            for(int j=0; j<n; j++){
                if(grid[i][j] == '1'){
                    res++;
                    dfs(grid, m, n, i, j);
                }
            }
        }
        return res;
    }
    
    /*
    DFS-Recursion
    rogram will automatically search all 4 possile directions as row index
	and y index is moving. Developer needs not to worry about it.
    */
    private static int[][] directions = new int[][]{{1,0}, {-1,0}, {0,1}, {0,-1}};
    
    private static void dfs(char[][] grid, int m, int n, int i, int j){
        if(i<0 ||i>=m ||j<0||j>=n||grid[i][j]!='1'){
            return;
        }
        grid[i][j] = '2';
        for(int[] direction: directions){
            int x = i+direction[0];
            int y = j+direction[1];
            dfs(grid, m, n, x, y);
        }
    }
    
    public static void main(String[] args) {
    	char[][] grid = new char[][] {{'1','1','0','0','0'},
						    		  {'1','1','0','0','0'},
						    	   	  {'0','0','1','0','0'},
						    		  {'0','0','0','1','1'},
						    		  {'0','0','0','0','1'}};
		System.out.println("Number of islands is "+numIslands(grid));
    }
}