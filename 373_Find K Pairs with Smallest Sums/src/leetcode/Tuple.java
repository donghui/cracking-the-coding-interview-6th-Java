package leetcode;

public class Tuple implements Comparable<Tuple>{
	/*
	 * In this case, x, y are indices for nums1 and nums2
	 * In LC378, x and y can be row index and column index
	 * of a sorted matrix
	 */
	int x, y, val;
	
	public Tuple(int x, int y, int val) {
		this.x = x;
		this.y = y;
		this.val = val;
	}

	@Override
	public int compareTo(Tuple o) {
		return this.val - o.val;
	}

}
