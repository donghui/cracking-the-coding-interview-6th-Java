package segment_tree;

public class NumArray {
	/*
	 * I will use the segment tree
	 */

	// Define SegmentTreeNode class, variables and initiations
	private static class SegmentTreeNode {
		int start;
		int end;
		int sum;
		SegmentTreeNode left;
		SegmentTreeNode right;

		public SegmentTreeNode(int start, int end, int sum) {
			this.start = start;
			this.end = end;
			this.sum = sum;
			this.left = null;
			this.right = null;
		}
	}

	SegmentTreeNode root;

	private static SegmentTreeNode buildTree(int[] nums, int start, int end) {
		if (nums == null || nums.length == 0)
			return null;

		if (start > end)
			return null;
		if (start == end) return new SegmentTreeNode(start, end, nums[start]);

		int mid = start + (end - start) / 2;
		SegmentTreeNode left = buildTree(nums, start, mid);
		SegmentTreeNode right = buildTree(nums, mid + 1, end);

		SegmentTreeNode root = new SegmentTreeNode(start, end, left.sum + right.sum);
		root.left = left;
		root.right = right;
		return root;
	}

	private static void update(SegmentTreeNode root, int i, int val) {
		if (root == null)
			return;
		if (i < root.start)
			return;
		if (i > root.end)
			return;

		// update sum at corresponding leaf.
		if (root.start == i && root.end == i) {
			root.sum = val;
			return;
		}

		update(root.left, i, val);
		update(root.right, i, val);

		// when exit recursion/backtracking, update sum at all internal nodes.
		root.sum = root.left.sum + root.right.sum;
	}

	private static int sumRange(SegmentTreeNode root, int i, int j) {
		if (root == null)
			return 0;
		if (i > root.end)
			return 0;
		if (j < root.start)
			return 0;

		i = Math.max(i, root.start);
		j = Math.min(root.end, j);

		if (root.start == i && root.end == j)
			return root.sum;

		int leftSum = sumRange(root.left, i, j);
		int rightSum = sumRange(root.right, i, j);

		return leftSum + rightSum;
	}

	public NumArray(int[] nums) {
		root = buildTree(nums, 0, nums.length-1);
	}

	public void update(int i, int val) {
		update(root, i, val);
	}

	public int sumRange(int i, int j) {
		return sumRange(root, i, j);
	}

	public static void main(String[] args){
    int[] nums = {2,4,5,7,8,9};
    NumArray obj = new NumArray(nums);
    System.out.println(obj.sumRange(1,3)); //16
    obj.update(0, 3);
    System.out.println(obj.sumRange(0,2)); //12
	}
}
