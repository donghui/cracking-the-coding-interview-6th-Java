package calculator;

import java.util.Stack;

public class Calculator {
	public static int calculate(String s) {
		if (s.length() == 0 || s == null) {
			return 0;
		}
		int num = 0;
		int res = 0;
		char lastOperator = '+';
		Stack<Integer> equ = new Stack<Integer>();
		
		for(int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			
			/*
			 * First extract numbers
			 * convert char to int by (char-'0')
			 */
			if(Character.isDigit(c)) {
				num = num*10+(c-'0');
				System.out.println("Now num is "+num);
			}
			
			/*
			 * by the end of last number, there should be operator.
			 * We don't consider ()
			 */
			if(!Character.isDigit(c) && c!=' ' || i == s.length()-1) {
				if(lastOperator == '+') {
					equ.push(num);
				} else if (lastOperator == '-') {
					equ.push(-num);
				} else if (lastOperator == '*') {
					equ.push(equ.pop()*num);
				} else if (lastOperator == '/') {
					equ.push(equ.pop()/num);
				}
				num = 0;
				lastOperator = c;
			}
		}
		
		while(!equ.isEmpty()) {
			res += equ.pop();
		}
		
		return res;
	}
	
	public static void main(String[] args) {
		String s = "-5 -6 +5 * 6";
		System.out.println(calculate(s));
		
	}

}
