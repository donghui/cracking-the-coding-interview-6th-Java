package twoPointers;

public class Solution {
	public static int minSubArrayLen(int s, int[] nums) {
		int res = Integer.MAX_VALUE;
		int total = 0;
		int slow = 0;
		
		for(int fast=0; fast<nums.length; fast++) {
			total += nums[fast];
			while(total >= s) {
				res = Math.min(res,  fast-slow+1);
				total -= nums[slow++];
			}
		}
		return res == Integer.MAX_VALUE? 0:res;
	}
	
	public static void main(String[] args) {
		int[] nums = {1,2,3,4,5};
		int s = 11;
		
		System.out.println("Minimum size of contiguous subarray whose sum exceeds "+s+" is "+minSubArrayLen(s, nums));
	}
}
