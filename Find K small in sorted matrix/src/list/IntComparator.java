package list;

public class IntComparator implements Comparable<IntComparator>{
	int val;
	public IntComparator(int val) {
		this.val = val;
	}
	
	@Override
	public int compareTo(IntComparator that) {
		return this.val - that.val;
	}
}
