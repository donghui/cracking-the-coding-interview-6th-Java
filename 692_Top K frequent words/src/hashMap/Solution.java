package hashMap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Solution {
public static List<String> topKFrequent(String[] words, int k) {
	/*
	 * First step: build word-count hashmap
	 */
        Map<String, Integer> wordKey = new HashMap<String, Integer>();
        for(String word: words) {
        	wordKey.put(word, wordKey.getOrDefault(word, 0)+1);
        }
        
        /*
         * The following list is what we need but it has warnings
         */
        //List<String> [] bucket = new List[words.length];
        
        /*
         * Build a list of String Array, the index is the count
         */
        List<Set<String>> wordCount = new ArrayList<Set<String>>(words.length);
        for(int i = 0; i < words.length; i++) {
        	wordCount.add(i, new HashSet<String>());
        }
        for(String word: words) {
        	int count = wordKey.get(word);
        	wordCount.get(count-1).add(word);
        }
        
        List<String> res = new ArrayList<String>();
        for(int i = wordCount.size()-1; i >= 0; i--) {
        	Set<String> elem = wordCount.get(i);
        	if(elem != null) {
        		List<String> list = new ArrayList<String>();
            	list.addAll(elem);
            	Collections.sort(list);
        		for(String obj: list) {
        			if(res.size() < k) {
        				res.add(obj);
        			}
        		}
        	}
        }
        return res;
    }

	public static void main(String[] args) {
		String[] words = {"i", "love", "leetcode", "i", "love", "coding"};
		List<String> res = topKFrequent(words, 2);
		for(String elem: res) {
			System.out.println(elem);
		}
	}
}
