import java.util.Queue;
import java.util.LinkedList;

public class Solution{
  public static int longestFilePath(String input){
    if(input == null || input.length() == 0) return 0;
    String[] inputs = input.split("\n");
    
    /* For each element in inputs, we want to know
    - Its level
    - Its length
    - Whether it's a file or a subfolder, we dont account for subfolder
    */
    Queue<nameAnalyzer> queue = new LinkedList<nameAnalyzer>();
    for(int i=0; i<inputs.length; i++){
      queue.offer(new nameAnalyzer(inputs[i]));
    }
    nameAnalyzer root = queue.poll();
    return dfs(queue, root, root.name.length());
  }
  
  /*
  Recursion: whether or not queue.poll() is the key. If current nameAnalyzer object has larger
  level than the parent nameAnalyzer object passed in the argument from last recursion, we do not
  poll the queue, and recursion will continue comparing the level of 1st element in queue with 
  the parent element in the argument, until finds its parent node
  */
  private static int dfs(Queue<nameAnalyzer> queue, nameAnalyzer node, int currentLen){
    /*
    Stopping criterion of recursion, the node is a file.
    The length is parent folder's absolute length+node's length+level (how many '/' are there)
    */
    if(node.isFile) return currentLen+node.level;
    if(queue.isEmpty()) return 0;
    
    int max=0;
    while(!queue.isEmpty()){
      nameAnalyzer child = queue.peek();
      if(child.level > node.level){
        queue.poll();
        max = Math.max(max, dfs(queue, child, currentLen+child.name.length()));
      } else {
        break;
      }
    }
    return max;
  }
  
  private static class nameAnalyzer{
    int level;
    String name;
    boolean isFile;

    nameAnalyzer(String input){
      int level = 0;
      int i = 0;
      for(i = 0; i < input.length(); i++) {
        if(input.charAt(i) == '\t') {
          level++;
        } else {
          break;
        }
      }
      this.level = level;
      this.name = input.substring(i);
      this.isFile = input.contains(".");
    }
  }
  
  public static void main(String[] args){
    String input = "dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext";
    System.out.println("Longest absolute file path is "+longestFilePath(input));
  }
}
