package findDupCahrIdx;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FindDuplicateCharIdx {
	public static List<Integer> findDuplicateCharIdx(String s, int n){
		if(s.length() == 0|| s == null) return null;
		
		/*
		 * map stores the index when its count starts to exceed n and thereafter.
		 * In the main() example, map is {0:3, 5:3, 5:4}
		 * res will be [0, 5, 5+1]
		 */
		List<Integer> res = new ArrayList<Integer>();
		char curr = s.charAt(0);
		int idx = 0;
		int count = 0;
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		for(int i = 0; i < s.length(); i++) {
			/*
			 * Process each char in s
			 */
			if(s.charAt(i) == curr) {
				count++;
			} else {
				curr = s.charAt(i);
				count = 1;
				idx = i;
			}
			
			if(count >= n) {
				map.put(idx, count);
			}
		}
		
		for(Integer i: map.keySet()) {
			for(int j = i; j <= i+map.get(i)-n; j++) {
				res.add(j);
			}
		}
		return res;
	}
	
	public static void main(String[] args) {
		List<Integer> res = findDuplicateCharIdx("111223333", 3);
		for(Integer i: res) {
			System.out.println(i);
		}
	}
}
