package findBadPlace;

public class FindBadPlace {
	/*
	 * Assume difference between good places are still 1
	 */
	public static int findBadPlace(int[] nums) {
		
		/*
		 * Will end up with e, mid, s
		 */
		int s = 0, e = nums.length-1;
		while(s+1<e) {
			int mid = s+(e-s)/2;
			int diffLeft = nums[mid] - nums[s];
			int diffRigh = nums[e] - nums[mid];
			
			if(diffLeft!=mid-s && diffRigh!=e-mid) {
				return mid;
			}else if(diffLeft!=mid-s) {
				e = mid;
			}else if(diffRigh!=e-mid) {
				s = mid;
			}
		}
		
		if(e != nums.length-1) {
			if(nums[e+1]-nums[e]==1) {
				return s;
			} else {
				return e;
			} 
		}else {//special case when e is nums.length-1
			if(nums[s]-nums[s-1] != 1) {
				return s;
			} else {
				return e;
			}
		}
	}
	
	public static void main(String[] args) {
		int[] nums = new int[] {7,8,12,13,14,15,16};
		System.out.println(findBadPlace(nums));
	}
}
