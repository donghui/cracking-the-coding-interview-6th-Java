package hashMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Solution {
	public static List<Integer> topKFrequent(int[] nums, int k){
		if(nums == null || nums.length == 0) {
			return null;
		}
		
		Map<Integer, Integer> numKey = new HashMap<Integer, Integer>();
		/*
		 * Build a HashMap
		 */
		for(int num: nums) {
			if(!numKey.containsKey(num)) {
				numKey.put(num, 1);
			} else {
				numKey.put(num,  numKey.get(num)+1);
			}
		}
		
		/*
		 * We then need to build an array of arraylist. arraylist is a list
		 * of num and its index is num's count. The largest possible count
		 * is nums.length. And count is at least 1.
		 */
		List<List<Integer>> numCount = new ArrayList<List<Integer>>();
		for (int i = 0; i < nums.length; i++) {
			numCount.add(i, new ArrayList<Integer>());
		}
		for(int num: nums) {
			int count = numKey.get(num);
			numCount.get(count-1).add(num);
		}
		
		/*
		 * Ready to output
		 */
		List<Integer> res = new ArrayList<Integer>();
		for(int i = numCount.size()-1; i >= 0; i--) {
			List<Integer> elem = numCount.get(i);
			if (elem != null) {
				for(int j = 0; j < elem.size(); j++) {
					if (res.size() < k && !res.contains(elem.get(j))) {
						res.add(elem.get(j));
					}
				}
			}
		}
		
		return res;
	}

	public static void main(String[] args) {
		int[] nums = {1};
		List<Integer> res = topKFrequent(nums, 1);
		for(int elem: res) {
			System.out.println(elem);
		}
	}
}
