
public class Solution {
	public static int maxDepth (TreeNode root) {
		if (root == null) return 0;
		return Math.max(maxDepth(root.left)+1, maxDepth(root.right)+1);
	}

	public static void main(String[] args) {
		TreeNode root = new TreeNode(3);
		TreeNode pointer = root;
		pointer.left = new TreeNode(9);
		pointer.right = new TreeNode(20);
		pointer = pointer.right;
		pointer.left = new TreeNode(15);
		pointer.right = new TreeNode(7);
		
		pointer = pointer.left;
		pointer.left = new TreeNode(11);
		
		int treeDepth = maxDepth(root);
		System.out.println("The maximum depth of the tree is "+treeDepth);
	}
}
