package dfs;

import java.util.ArrayList;
import java.util.List;

public class Solution {
	public static List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> res = new ArrayList<List<Integer>>();
        if(nums==null || nums.length == 0) return res;
        dfs(nums, res, new ArrayList<Integer>());
        return res;
    }
    
    private static void dfs(int[] nums, List<List<Integer>> res, ArrayList<Integer> list){
        if (list.size() == nums.length) {
            res.add(new ArrayList<Integer>(list));
//            System.out.println(list.toString());
            System.out.println(res.toString());
        }
        
        for(int i=0; i<nums.length; i++){
            if(list.contains(nums[i])){
                continue;
            }
            list.add(nums[i]);
            dfs(nums, res, list);
            list.remove(list.size()-1);
        }
    }
    
    public static void main(String[] args) {
    	int[] nums = {1,2,3};
    	List<List<Integer>> res = permute(nums);
    	System.out.println(res.toString());
    }
}
