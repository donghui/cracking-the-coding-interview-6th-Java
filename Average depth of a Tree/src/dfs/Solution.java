package dfs;

import java.util.ArrayList;
import java.util.List;

import bfs.TreeNode;

public class Solution {
	public static double findDepthAverage(TreeNode root) {
		//Treat special case
		if(root == null) {
			return 0;
		}
		
		//Recursively goes to each leaf directly and get the depth, and add depth to depths
		List<Integer> depths = new ArrayList<Integer>();
		dfs(root, 0, depths);
		
		//Just calculate the average
		int total = 0;
		for(int depth: depths) {
			total += depth;
		}
		return (double) total/depths.size();
	}

	/*
	 * DFS: depth first search. We recursively arrive at each leaf. Every time we proceed
	 * to the next level of the tree recursively, we increase the depth by 1. And the the leaf
	 * we add depth to List called depths.
	 */
	private static void dfs(TreeNode root, int depth, List<Integer> depths) {
		depth++;
		
		//We arrived at a leaf
		if(root.left == null && root.right == null) {
			depths.add(depth);
		}
		
		//We proceed to left child
		if(root.left != null) {
			dfs(root.left, depth, depths);
		}
		//We proceed to right child
		if(root.right != null) {
			dfs(root.right, depth, depths);
		}
	}
	
	public static void main(String[] args) {
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(1);
		root.right = new TreeNode(1);
		root.left.left = new TreeNode(1);
		root.right.right = new TreeNode(1);
		root.left.left.left = new TreeNode(1);
		root.right.right.right = new TreeNode(1);
		
		System.out.println("Average depth of the tree is "+findDepthAverage(root));
	}
}
