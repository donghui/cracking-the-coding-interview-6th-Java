package breath_first_search;

import java.util.Arrays;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class Solution {
	public static int racecar(int target) {
		
		/*
		 * queue stores the tree nodes ([position, speed])
		 * and once the head is used to calculate children via A or R, it will be removed
		 */
		Deque<int[]> queue = new LinkedList<>();
		queue.offerLast(new int[] {0,1}); //the root node is {0,1}
		
		Set<String> visited = new HashSet<>();//String is easy to compare
		visited.add(Arrays.toString(queue.getFirst())); //root is visited already
		
		int level = 0;
		while(!queue.isEmpty()) {
			int size = queue.size(); //in for-loop, queus.size() is subject to change.
			for(int i =0; i<size; i++) {
				int[] current = queue.pollFirst(); //retrieve and remove the head
				
				if (current[0] == target) {//We reached the targe!
					return level;
				}
				
				int[] acce = new int[] {current[0]+current[1], current[1]*2};//acceleration.*2 is the same as <<1
				String key = Arrays.toString(acce);
				/*
				 * The {pos,speed} pair should not be visited before to guarantee shorted length
				 * pos should not be larger than 2*target, because if we find we are already too far
				 * from the target, we should return or stop.
				 */
				if(!visited.contains(key) && acce[0]>0 && acce[0] < 2*target) {
					queue.offerLast(acce);
					visited.add(key);
				}
					
				int[] reve = new int[] {current[0], current[1]>0? -1:1}; //reverse
				key = Arrays.toString(reve);
				if(!visited.contains(key) && reve[0]>0 && reve[0] < 2*target) {
					queue.offerLast(reve);
					visited.add(key);
				}
			}			
			level++;
		}
		return -1;
	}
	
	public static void main(String[] args) {
		int target = 3;
		int length = racecar(target);
		System.out.println("The shortest instruction sequence is "+length);
		
		target = 6;
		length = racecar(target);
		System.out.println("Now the shortest instruction sequence is "+length);
	}

}
