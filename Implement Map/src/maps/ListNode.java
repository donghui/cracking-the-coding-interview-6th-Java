package maps;

public class ListNode {
	int key;
	int value;
	ListNode next;
	
	public ListNode(int key, int val) {
		this.key = key;
		this.value = val;
	}
}
