package leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;

public class Solution {
	public static List<int[]> kSmallestPairs(int[] nums1, int[] nums2, int k){
		PriorityQueue<Tuple> pq = new PriorityQueue<Tuple>();
		int m = nums1.length, n = nums2.length;
		List<int[]> res = new ArrayList<int[]>();
		
		if(nums1 == null || nums1.length == 0 || nums2 == null || nums2.length == 0 || k <= 0) return res;
		
		/*
		 * Basically it is a mxn sorted matrix
		 * First offer first row to queue. 
		 * The reason of doing this is because, we can imagine the queue of length n
		 * is moving like a snake in row-wise. The entry next to the tail (largest)
		 * of the queue is just below the head of the queue in the same column.
		 */
		for(int j = 0; j <= n-1; j++) pq.offer(new Tuple(0, j, nums1[0]+nums2[j]));
		
		/*
		 * 
		 */
        for(int i = 0; i < Math.min(k, m *n); i++) {
            Tuple t = pq.poll();
            res.add(new int[]{nums1[t.x], nums2[t.y]});
            if(t.x == m - 1) continue;
            /*
             * This is the entry next to the tail of the queue I mentioned before
             */
            pq.offer(new Tuple (t.x + 1, t.y, nums1[t.x + 1] + nums2[t.y]));
        }
        return res;
	}
	
	public static void main(String[] args) {
		int[] nums1 = {1,3,4,7,8};
		int[] nums2 = {1,2,3,4};
		List<int[]> res = kSmallestPairs(nums1, nums2, 3);
		for (int[] elem: res) {
			System.out.println(Arrays.toString(elem));
		}
		
	}

}
