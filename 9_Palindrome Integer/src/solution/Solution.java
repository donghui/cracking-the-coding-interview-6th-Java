package solution;

public class Solution {
	public static boolean isPalindrome(int num) {
		if (num < 0) return false;
		
		int div = 1;
		while(num/div >= 10) {
			div *= 10;
		}
		
		while (num != 0) {
			int hi = num/div;
			int lo = num%10;
			
			if (hi != lo) return false;
			
			num -= (num/div)*div;
			num /= 10;
			
			div /= 100;
		}
		
		return true;
	}
	
	public static void main (String[] args) {
		int num = 1234421;
		
		boolean check = isPalindrome(num);
		if (check) {
			System.out.println(num+" is Palindrome!");
		} else {
			System.out.println(num+" isn't Palindrome!");
		}	
	}
}
