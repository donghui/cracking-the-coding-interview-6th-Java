/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */

public class Solution {
	public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		if(l1==null && l2==null) {
			return null;
		}
		
		int carry = 0;
		ListNode result = new ListNode(0);
		ListNode pointer = result;
		
		while(l1 != null && l2 != null) {
			int sum = l1.val+l2.val+carry;
			carry = sum / 10;
			sum = sum % 10;
			pointer.next = new ListNode(sum);
			l1 = l1.next;
			l2 = l2.next;
			pointer = pointer.next;
		}
		
		while(l1 != null) {
			int sum = l1.val+carry;
			carry = sum / 10;
			sum = sum % 10;
			pointer.next = new ListNode(sum);
			l1 = l1.next;
			pointer = pointer.next;
		}
		
		while(l2 != null) {
			int sum = l2.val+carry;
			carry = sum / 10;
			sum = sum % 10;
			pointer.next = new ListNode(sum);
			l2 = l2.next;
			pointer = pointer.next;
		}
		
		if (carry != 0) {
			pointer.next = new ListNode(carry);
			pointer = pointer.next;
		}
		
		return result.next;
	}
	
	private static ListNode array2List(int[] arrays) {
		ListNode pointer;		
		if (arrays.length > 0) {
			ListNode l1 = new ListNode(arrays[0]);
			pointer = l1;
			for(int i = 1; i < arrays.length; i++) {
				pointer.next = new ListNode(arrays[i]);
				pointer = pointer.next;
			}
			return l1;
		}	else {
			return null;
		}
	}
	
	public static void main (String[] args) {
		int[] array1 = {2,4,3};
		ListNode l1 = array2List(array1);
		
		int[] array2 = {5,6,4};
		ListNode l2 = array2List(array2);
		
		ListNode results = addTwoNumbers(l1, l2);
		
		//Naive but straightforward way to check results
		System.out.println(results.val);
		System.out.println(results.next.val);
		System.out.println(results.next.next.val);
	}
}
