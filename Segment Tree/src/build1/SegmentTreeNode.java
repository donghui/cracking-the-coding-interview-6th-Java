package build1;

public class SegmentTreeNode {
	int start, end;
	SegmentTreeNode left, right;
	
	SegmentTreeNode(int start, int end){
		this.start = start;
		this.end = end;
		this.left = null;
		this.right = null;
	}
}
