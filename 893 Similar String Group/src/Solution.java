
public class Solution {
	public static int numSimilarGroups(String[] A) {
		if(A.length < 2) return A.length; //0 or 1 group
        
		int res = 0;
		for(int i = 0; i < A.length; i++) {
			if(A[i] == null) continue;
			String temp = A[i];
			A[i] = null; //after assigning A[i] to a group, make it null
			res++; //increase the number of groups by 1
			dfs(A, temp);
		}
		return res;
    }
	
	//dfs converts strings in A to null if they belong to the same group as p
	private static void dfs(String[] A, String p) {
		for (int i=0; i<A.length; i++) {
			if(A[i]==null) continue;
			if(isSameGroup(A[i],p)) {
				String q = A[i];
				A[i] = null;
				dfs(A, q); //recursion to find String swap only 2 letters from q.
			}
		}
	}
	
	private static boolean isSameGroup(String p, String s) {
		int res = 0, i = 0;
		while(res <=2 && i < p.length()) { //p or s is OK; if res > 2, stop!
			if(p.charAt(i) != s.charAt(i)) res++;
			i++;
		}
		return res == 2;
	}
	
	public static void main(String[] args) {
		String[] A = {"tars", "rats", "arts", "star"};
		int res = numSimilarGroups(A);
		System.out.println("There are "+res+" groups!");
	}
}
